import time
import json
import yaml
import xmltodict
import pickle
import msgpack
from dicttoxml import dicttoxml


def serde(ser_type, data, serialization_func, deserialization_func):
    print("\n\nSerializing & deserializing {}".format(ser_type))
    
    start = time.time()
    serialized = serialization_func(data)
    afterser = time.time()
    deserialized = deserialization_func(serialized)

    #For xml, one can use normal dict instead of OrderedDict
    # deserialized = deserialization_func(serialized, dict_constructor=dict)
    
    #For msgpack, raw=False won't use binary strings
    #deserialized = deserialization_func(serialized, raw=False)

    end = time.time()
    print("Serialization: {}, deserialization:{}, delta {}\n".format(afterser - start, end - afterser, end - start))
    print("Serialized\n", serialized)
    print("\nDeserialized\n", deserialized)

    #Run multiple serializations for more accurate readings
    # n_serializations = time.time()
    # for i in range(0, 1000):
    #     n_seralized = serialization_func(data)
    #     n_deserialized = deserialization_func(serialized)
    # n_serializations_end = time.time()
    # print(n_serializations_end - n_serializations)
    
    
def main():
    vehicles = {
        "root": {
            "cars": {
                "Ford": ["Mustang", "Fiesta"],
                "Lada": "VAZ-2105s"
            },
            "planes": {
                "US": {
                    "Lockheed Marting": "F22",
                    "Boeing": "747"
                },
                "Russia": {
                    "Sukhoi": "Su-27",
                    "Mikoyan": "MiG-29"
                }
            }
        }
    }
    
    serde("YAML", vehicles, yaml.dump, yaml.load)
    serde("XML", vehicles, dicttoxml, xmltodict.parse)
    serde("JSON", vehicles, json.dumps, json.loads)
    serde("MSGPACK", vehicles, msgpack.packb, msgpack.unpackb)
    serde("PICKLE", vehicles, pickle.dumps, pickle.loads)
    

if __name__ == "__main__":
    main()
